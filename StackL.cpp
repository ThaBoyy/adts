#include "StackL.h"
#include <iostream>

using namespace std;


int Stack::size()
{
    int y = data.size();
    
    return y;
}

void Stack::push(int a)
{
    if (size() == 0)
    {
        data.insert(a, 1);
    }
    
    else
    {
        data.insert(a, size() + 1);
    }
}

void Stack::pop()
{
    data.remove(size());
}

int Stack::top()
{
    int t = data.get(size());
    return t;
}

void Stack::clear()
{
    while (size() > 0)
    {
        pop();
    }
}
