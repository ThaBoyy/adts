#include <iostream>
#include "List.h"

using namespace std;

int main()
{
    // Declare two list objects, L1 and L2
    List L1;
    List L2;

 cout << "Welcome To My List ADT client" << endl << endl;

 // Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
 // ...
 
    cout << "The Size Of List 1 Is: " << L1.size() << endl;
    cout << "The Size Of List 2 Is: " << L2.size() << endl;
    L1.clear();
    L2.clear();

}
