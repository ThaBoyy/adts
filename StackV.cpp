#include "StackV.h"
#include <iostream>

using namespace std;

int Stack::size() // For Each Loop That Will Increment y For Each Value
{
    int y = 0;
    for ( int x : data)
    {
        y++;
    }
    return y;
}

void Stack::push(int a)
{
    int s = size();
    int n = s + 1; // New Size For Vector
    vector <int> d2(n); // New Vector With New Size
    
    if (size() == 0) // Special Case For Empty Vector
    {
        d2[0] = a;
    }
    
    else
    {
        for (int p = 0; p < n; p++) // Loop To Copy Over Data To New Vector
        {
            d2[p] = {data[p]};
        }
    }
    
    d2[s] = a; // Assigning The New Value To Last Position
    data = {d2}; // Rewriting Old List To New List
}

void Stack::pop()
{
    int b = size() - 1; // New Size For Vector
    vector <int> pop(b); // New Vector With New Size
    
    for (int p = 0; p < b; p++) // Loop To Copy Over Data To New Vector
    {
        pop[p] = {data[p]};
    }
    
    data = {pop}; // Rewriting Old List To New List
}

int Stack::top()
{
    int t = size() - 1;
    return data[t];
}

void Stack::clear()
{
    for (int c = size(); c > 0; c--)
    {
        pop();
    }
    
    cout << "Vector Has Been Cleared." << endl;
}
