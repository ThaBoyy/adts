#include <iostream>
#include <stdexcept> // Used To Be Able To "throw" Exceptions
using namespace std;

#ifndef LIST_H
#define LIST_H

class List // Begin List Definition
{
  private:
    class Node; // Forward declaration (defined in the implementation file)
    
    Node* frontPtr = nullptr;
    int num_elements = 0;
    
  public:
     ~List(); // Destructor
     void insert(int element, int k); // insert element at location k
     void remove(int k); // remove element at location k
     int size(); // return the number of elements in the List
     
     
     /** MISSING OPERATIONS */
     
        int get(int k); // To Return The K-th Element
        void clear(); // To Delete All Elements

}; // End List Definition

#endif
