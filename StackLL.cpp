#include "StackLL.h"
#include <iostream>

using namespace std;

class Stack::Node
{
    public:
    int data = 0;
    Node* link = nullptr;
};

Stack::~Stack()
{
    clear();
}

int Stack::size()
{
    int s = num_elements;
    return s;
}

void Stack::push(int a)
{
    Node* NN = new Node{a};
    
    if (size() == 0)
    {
        frontPtr = NN;
    }
    
    else
    {
        Node* newPtr = frontPtr;
        int s = size();
        
        for (int i = 1; i < s; i++) // while (newPtr->link != nullptr)
        {
            newPtr = newPtr->link;
        }
        
        newPtr->link = NN;
    }
    
    num_elements++;
}

void Stack::pop()
{
    Node* pPtr = frontPtr;
    
    if (size() == 1)
    {
        delete pPtr;
    }
    
    else
    {
        int k = size();
        int p = 1;
    
        while (p < k)
        {
            pPtr = pPtr->link;
            p++;
        }
    
        delete pPtr;
    }
    
    num_elements--;
}

int Stack::top()
{
    Node* tPtr = frontPtr;
    int t = size();
    
    for (int n = 1; n < t; n++)
    {
        tPtr = tPtr->link;
    }
    
    int ans = tPtr->data;
    return ans;
}

void Stack::clear()
{
    for (int i = num_elements; i > 0; i--)
    {
        pop();
    }
}
